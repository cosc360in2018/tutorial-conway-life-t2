# Tutorial 2

For the second tutorial, we're going to work with SVG, and CSS via SASS.

The tutorial is quite short, because you have another task: choosing a game you're going to build and thinking about the data structures you will need to model it.

## Clone the code

First, get the code and put it somewhere you can load it in your browser. 

To get the code, open a terminal, `cd` into the public directory of whichever webserver you are using, and:

```sh
git clone https://gitlab.une.edu.au/cosc360in2018/tutorial-conway-life-t2.git
```

This will clone this code using the git version control system. That will, amongst other things, let you switch between different branches (eg, the solution and the start of the exercise)

## Check it's working

Open `index.html` in the browser, and you should find yourself faced with Conway's Game of Life from Tutorial 1.

Time to open the code. I recommedn Visual Studio Code as an editor for this.

## Change the game to use SVG

Your first challenge is to change the game so that instead of using a grid of dots and hashes, it uses SVG elements to render the game a little more prettily.

Then take a look in index.html. This contains a `div` element where the game board is going to be rendered:

```html
<div id="game" style="font-family: monospace; cursor: pointer;">
</div>
```

We're going to need to change that to an svg element

```html
<svg xmlns="http://www.w3.org/2000/svg" id="game" width="640" height="400"></svg>
```

Next, we need to alter render.js

1. Let's name a couple of constants.

    ```js
    let xmlns = "http://www.w3.org/2000/svg"
    let cellSize = 20
    ```

2. In render, in the outer loop, change the rows it inserts from `div` elements to SVG `g` elements. Remember you'll need to use `document.createElementNS` and give it the SVG namespace

3. In render, in the inner loop, change the cells it inserts from `span` elements to SVG `rect` elements. Again you'll need to use the namespace. Set the attributes on each `rect`:

    ```js
    t.setAttribute("x", x * cellSize)
    t.setAttribute("y", y * cellSize)
    t.setAttribute("width", cellSize)
    t.setAttribute("height", cellSize)
    t.classList.add("cell")
    ```

4. The code that set `innerHTML` on the old `span` elements isn't needed. Instead, add the `alive` class to each `rect` if the corresponding cell is alive.

Reload the page, and you will probably see a large black rectangles. We have our rectangles, but they are all black and unstyled.

For the moment, let's do the minimum to get the game rendering nicely. In `index.html` paste the following stylesheet tag:

```html
<style>
    .cell {
      fill: #ddd;
      stroke: #444;
      cursor: pointer;
    }
    .cell.alive {
      fill: #88f;
    }
</style>
```

Now reload the page, and you should have a slightly prettier Conway's Game of Life. (But only slightly)

## Getting SASS

The next part of the tutorial is overkill for such a simple example, but it'll serve as a light introduction to NPM and to preprocessors.

Node.js is a JavaScript engine for servers. Node Package Manager (NPM) has become a default way of installing a lot of different preprocessing tools. On turing, Node and NPM are already installed, but if you are working on your local machine then install them.

Next, we're going to install SASS just locally in this directory (on turing, you don't have permission to install node modules globally).

From a terminal, in the project directory, let's first initialise `package.json`

```sh
npm init
```

Hit enter to every default. You're just doing this to see what it creates. Have a look at the file that has now appeared, describing your little client-side project.

Now let's install sass

```
npm install sass --save-dev
```

There should now be a `node_modules` directory, and in `package.json` you should have a dependency on sass.

For the moment, that's enough on npm. We'll just use the sass.js that it's installed. It's in `node_modules/sass/sass.js`

Try running it from the shell, and see what it says.

## Using SASS

First, let's alter `index.html` to use a linked stylesheet. Delete the `style` element and contents, and in its place put

```html
<link rel="stylesheet" href="style.css" />
```

Only what we're going to create first is `style.scss`

In style.css, first let's declare some variables

```scss
$primary-color: #88f;
$empty-color: #ddd;
$border-color: #444;
```

Then, let's declare the cell class to use them

```scss
.cell {
  fill: $empty-color;
  stroke: $border-color;
  cursor: pointer;

}
```

And finally, let's *nest* selectors to specialise cells if they are alive or hovered. Normally, nesting selectors inside the `.cell` selector would select dependents. But if we preceed the selectors with `&` we can select for cells that have additional classes or pseudoclasses. Nest this inside the block for the cell class

```scss
  &:hover {
    fill: darken($empty-color, 10%);  
  }
  &.alive {
    fill: $primary-color;
  }
  &.alive:hover {
    fill: darken($primary-color, 10%);
  }
```

We've now used some SCSS features to describe our styles. It's time to use SASS to preprocess it.

First, let's just run SASS and see what it converts it to

```sh
node_modules/sass/sass.js style.scss
```

That should print out some derived CSS on the terminal.

Let's get it to put it in style.css

```sh
node_modules/sass/sass.js style.scss style.css
```

And now if we reload the our page, our Conway Game of Life should have got slightly prettier again, as hovering over cells just hints them slightly darker.

## Why we don't do a lot of CSS

In this tutorial, you've largely been introduced to SASS to introduce NPM, styling SVG elements, and the idea of a pre-processor.

In practice, if you're looking to develop an initial prototype quite quickly, developers don't write their own CSS from scratch. In many cases, they start by using classes from a CSS framework such as Bootstrap. [http://getbootstrap.com](http://getbootstrap.com)

It provides CSS classes for attractive common widgets, such as button groups, alerts, etc.

